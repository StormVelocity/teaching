#!/usr/bin/env python3

def choose(choice_text, choices):
    '''
    Function to abstract making a choice.
    '''

    while True:
        choice = input(choice_text + " >> ").lower()

        if choice in choices:
            break

    return choice

choice = choose("Type 'Yellow' to go left or 'Blue' to go right.", ['blue', 'yellow'])

if choice == "yellow":
    print("You so yellow!")
else:
    print("You so blue!")
