#! /usr/bin/env python3

from PIL import Image

def print_pixels(filename):

    im = Image.open(filename)
    pixels = im.load()

    max_x, max_y = im.size

    # Just here as an example.
    single_array = list(im.getdata())

    for x in range(max_x):
        for y in range(max_y):
            print("pixel %d,%d RGB value: %s" % (x, y, pixels[x,y]))

if __name__ == "__main__":
   print_pixels("fuzz.jpg")
