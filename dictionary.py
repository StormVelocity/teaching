#! /usr/bin/env python3

awesome_dict = dict()

awesome_dict["Devin"] = "oh god yeah"
awesome_dict[7] = "naw, 7 is cool"
'''

print("Does Devin awesome? %s" % (awesome_dict["Devin"]))
print("How about the number 7? %s" % (awesome_dict[7]))


print("keys: %s" % (awesome_dict.keys()))
print("values: %s" % (awesome_dict.values()))
print("items: %s" %(awesome_dict.items()))


my_list = []

my_list.append("fidibity")
my_list.append("gibletz")

print(my_list[0])
print(my_list[1])
print(my_list)
'''

print("dictionary iteration example")

for key in iter(awesome_dict):
    print("the key is %s" % key)
    print("%s:%s" % (key, awesome_dict[key]))

